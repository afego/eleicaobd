/*=======================
0 - Inicializacao
=======================*/

create database ELEICAO;
use ELEICAO;
#drop database ELEICAO;

#Tabela que armazena os codigos das unidades federativas
#Utilizado como dominio em CANDIDATO.UF e ZONAELEITORAL.UF
create table CODUF(
Codigo char(2) not null,
Nome varchar(20) not null,

constraint PK_CODUF
    primary key(Codigo),
constraint SK_CODUF
    unique(Nome)
);
insert into CODUF(Codigo,Nome) values
    ('AC','Acre'),
    ('AL','Alagoas'),
    ('AP','Amapa'),
    ('AM','Amazonas'),
    ('BA','Bahia'),
    ('CE','Ceara'),
    ('DF','Distrito Federal'),
    ('ES','Espirito Santo'),
    ('GO','Goias'),
    ('MA','Maranhao'),
    ('MT','Mato Grosso'),
    ('MS','Mato Grosso do Sul'),
    ('MG','Minas Gerais'),
    ('PA','Para'),
    ('PB','Paraiba'),
    ('PR','Parana'),
    ('PE','Pernambuco'),
    ('PI','Piaui'),
    ('RJ','Rio de Janeiro'),
    ('RN','Rio Grande do Norte'),
    ('RS','Rio Grande do Sul'),
    ('RO','Rondonia'),
    ('RR','Roraima'),
    ('SC','Santa Catarina'),
    ('SP','Sao Paulo'),
    ('SE','Sergipe'),
    ('TO','Tocantins');

#Tabela que armazena os titulos dos cargos
#Utilizado na entidade CANDIDATO
create table CODCARGO(
Codigo char(2) not null,
Titulo varchar(19) not null,

constraint PK_CODCARGO
    primary key(Codigo),
constraint SK_CODCARGO
    unique(Titulo)
);
insert into CODCARGO(Codigo,Titulo) values
    ('PE','Presidente'),
    ('GO','Governador'),
    ('SE','Senador'),
    ('DF','Deputado Federal'),
    ('DE','Deputado Estadual'),
    ('PO','Prefeito'),
    ('VE','Verador');

/*=======================
1 - ENTIDADES
=======================*/

#1.F - ZONA ELEITORAL
create table ZONAELEITORAL(
UF char(2) not null,
Codigo int not null,

constraint PK_ZONAELEITORAL
    primary key(UF,Codigo)
);

#1.G - SECAO ELEITORAL
create table SECAOELEITORAL(
Codigo int not null,
Endereco varchar(30) not null,

constraint PK_SECAOELEITORAL
    primary key(Codigo)
);

#1.D - PARTIDO
create table PARTIDO(
Nome varchar(20) not null,
Codigo char(2) not null,

constraint PK_PARTIDO
    primary key(Codigo),
constraint SK_PARTIDO
    unique(Nome)
);

#1.E - CARGO ELETIVO
create table CARGOELETIVO(
Titulo char(2) not null,

constraint PK_CARGOELETIVO
    primary key(Titulo)
);

#1.A - ELEITOR
create table ELEITOR(
PNome varchar(15) not null,
MNome varchar(15),
UNome varchar(15) not null,
RG char(9) not null,
DNascimento date,
CEP char(8) not null,
Numero int,
Complemento varchar(20),
#2.A - VOTA NO LOCAL
#Adicionando os atributos em ELEITOR que representam a relacao VOTA NO LOCAL
ZUF char(2) not null,
ZCodigo int not null,
SCodigo int not null,

constraint PK_ELEITOR
    primary key(RG),
constraint FK_ELEITOR_ZONA 
	foreign key(ZUF,ZCodigo) references ZONAELEITORAL(UF,Codigo)
        on update cascade,
constraint FK_ELEITOR_SECAO 
	foreign key(SCodigo) references SECAOELEITORAL(Codigo)
        on update cascade
);
        
#1.B - CANDIDATO
create table CANDIDATO(
Codigo varchar(7) not null,
UF char(2) not null,
CanRG char(9) not null,
NomeCandidatura varchar(20),
#2.C - FAZ PARTE      
#Adicionando o atributo Codigo de PARTIDO em CANDIDATO, para representar a relacao FAZ PARTE
PCodigo char(2) not null,
#2.D - CONCORRE
#Adicionando atributo Titulo de CARGOELETIVO, para representar a relacao CONCORRE
Titulo char(2) not null,

constraint PK_CANDIDATO
    primary key(CanRG),
constraint FK_CANDIDATO_CanRG
    foreign key(CanRG) references ELEITOR(RG),
constraint SK_CANDIDATO
    unique(Codigo,UF),
constraint FK_CANDIDATO_PCodigo foreign key(PCodigo) references PARTIDO(Codigo)
        on update cascade,
constraint FK_CANDIDATO_Titulo foreign key(Titulo) references CARGOELETIVO(Titulo)
);
        
#1.C - MESARIO    
create table MESARIO(
Nome varchar(15) not null,
RG char(9) not null,
#2.G - TRABALHA
#Adicionando os atributos em MESARIO que representam a relacao TRABALHA
ZUF char(2) not null,
ZCodigo int not null,
SCodigo int not null,

constraint PK_MESARIO
    primary key(RG),
constraint FK_MESARIO_RG
    foreign key(RG) references ELEITOR(RG),
constraint FK_MESARIO_ZONA foreign key(ZUF,ZCodigo) references ZONAELEITORAL(UF,Codigo)
        on update cascade,
constraint FK_MESARIO_SECAO foreign key(SCodigo) references SECAOELEITORAL(Codigo)
        on update cascade
);

#1.H - HISTORICO VOTOS
create table HISTORICOVOTOS(
Ano char(4) not null,
Turno int not null,
Votos int not null,
CanRG char(9) not null,
ZUF char(2) not null,
ZCodigo int not null,
Titulo char(2) not null,
PCodigo char(2) not null,
constraint PK_HISTORICOVOTOS
    primary key(Ano,Turno,CanRG,ZUF,ZCodigo),
check(Turno>=1 and Turno<=2)
);

create table variavelcontrole (
Controle int, 
Ano char(4),
controleano int,
check(Controle>=0 and Controle<=1)
);
insert into variavelcontrole values (0,'0000',0);

#historico(ano,canrg,titulo,votos,uf,zona,pcodigo)
create table HISTORICO(
Ano char(4) not null,
CanRG char(9) not null,
Titulo char(2) not null,
votos int not null,
UF char(2) not null,
Zona int not null,
PCodigo char(2) not null,

constraint PK_HISTORICO
	primary key(Ano,CanRG,Titulo)
);
/*=======================
2 - RELACOES
=======================*/
    
#2.B - VOTA EM
create table VOTAEM(
    Ano char(4) not null,
    Turno char(1) not null,
    ERG char(9) not null,
    CanRG char(9) not null,
    ZUF char(2) not null,
    ZCodigo int not null,
    
    constraint PK_VOTAEM
        primary key(Ano,Turno,ERG,CanRG,ZUF,Zcodigo),
    constraint FK_VOTAEM_ERG
        foreign key(ERG) references eleitor(RG),
    constraint FK_VOTAEM_CANRG
        foreign key(CanRG) references candidato(CanRG),
    constraint FK_VOTAEM_ZONA
        foreign key(ZUF,ZCodigo) references zonaeleitoral(UF,Codigo)
        
);

#2.F - CONTEM
create table CONTEM(
ZUF char(2) not null,
ZCodigo int not null,
SCodigo int not null,

constraint PK_CONTEM
    primary key(ZUF,ZCodigo,SCodigo),
constraint FK_CONTEM_ZONA
    foreign key(ZUF,ZCodigo) references zonaeleitoral(UF,Codigo)
        on delete cascade on update cascade,
constraint FK_CONTEM_SECAO
    foreign key(SCodigo) references secaoeleitoral(Codigo)
    on delete cascade on update cascade
);

/*=======================
3 - TRIGGERS
=======================*/

DELIMITER $
#Domínio da UF para ZONAELEITORAL
create trigger Checa_UF_ZONA_INSERT
before insert on ZONAELEITORAL
for each row
begin
    if NEW.UF not in (select Codigo from CODUF)then
		signal sqlstate '32658' set message_text='O Estado a ser inserido não existe no dominio';
	end if;
end $
create trigger Checa_UF_ZONA_UPDATE
before update on ZONAELEITORAL
for each row
begin
    if NEW.UF not in (select Codigo from CODUF)then
		signal sqlstate '32658' set message_text='O Estado a ser inserido não existe no dominio';
	end if;
end $
/*
#Domínio da UF para CANDIDATO
create trigger Checa_UF_CANDIDATO
before insert on CANDIDATO
for each row
begin
    if NEW.UF not in (select Codigo from CODUF)then 
		signal sqlstate '32658' set message_text='A UF a ser inserida não existe';
	end if;
end $
*/
#RESTRIÇÃO SEM NTICA: Um candidato não pode candidatar-se em uma UF onde não reside.
create trigger checa_UF_consistencia_INSERT
before insert on candidato
for each row
begin
    if NEW.UF not in (select ZUF from eleitor where new.CanRG=eleitor.RG)then 
		signal sqlstate '32657' set message_text='O Candidato não reside no UF onde vota.';
	end if;
end $
create trigger checa_UF_consistencia_UPDATE
before update on candidato
for each row
begin
    if NEW.UF not in (select ZUF from eleitor where new.CanRG=eleitor.RG)then 
		signal sqlstate '32657' set message_text='O Candidato não reside no UF onde vota.';
	end if;
end $

#Dominio de CARGOELETIVO
create trigger checa_CARGOELETIVO_INSERT
before insert on CARGOELETIVO
for each row
begin
    if NEW.Titulo not in (select Codigo from CODCARGO)then 
		signal sqlstate '32659' set message_text='O cargo eletivo especificado não existe.';
	end if;
end $
create trigger checa_CARGOELETIVO_UPDATE
before update on CARGOELETIVO
for each row
begin
    if NEW.Titulo not in (select Codigo from CODCARGO)then 
		signal sqlstate '32659' set message_text='O cargo eletivo especificado não existe.';
	end if;
end $

#RESTRIÇÃO SEMANTICA: Um eleitor só pode votar em um candidato, que não seja para presidente, em sua mesma UF
create trigger checa_ZONA_CARGO
before insert on VOTAEM
for each row
begin
    if  exists (select * from CANDIDATO where (new.CanRG=CANDIDATO.CanRG and CANDIDATO.Titulo!='PE'))
    and new.ZUF not in(select UF from CANDIDATO where new.CanRG=CanRG) then 
		signal sqlstate '32660' set message_text='A UF de onde o voto é feito é diferente da UF onde reside';
	end if;
end $
create trigger checa_ZONA_CARGO_UPDATE
before update on VOTAEM
for each row
begin
    signal sqlstate '32660' set message_text='Voce nao pode modificar um voto feito';
end $

#RESTRIÇÃO SEMANTICA: Um eleitor não pode votar dentro de uma zona que não é sua
create trigger  checa_voto_zona
before insert on VOTAEM
for each row
begin
	if new.ZUF<>(select ZUF from ELEITOR where new.ERG=ELEITOR.RG) 
    or new.ZCodigo<>(select ZCodigo from ELEITOR where new.ZCodigo=ELEITOR.ZCodigo and new.ERG=ELEITOR.RG )then
		signal sqlstate '32661' set message_text='O eleitor está votando fora de sua zona ou UF';
    end if;
end $

#Antes de deletar os votos, contabilizamos todos eles
create trigger votos_por_zona
before delete on VOTAEM
for each row
begin
	declare part char(2);
    declare cargo char(2);
    if not exists(select * from HISTORICOVOTOS where ano=old.ano and turno=old.turno and CanRG= old.CanRG and ZUF=old.ZUF and ZCodigo=old.ZCodigo) then
        set part= (select PCodigo from CANDIDATO where old.CanRG=CanRG);
        set cargo= (select Titulo from CANDIDATO where old.CanRG=CanRG);
        insert into HISTORICOVOTOS(Ano,Turno,Votos,CanRG,ZUF,ZCodigo,Titulo,PCodigo) 
			values(old.ano,old.turno,0,old.CanRG,old.ZUF,old.ZCodigo,cargo,part);
        set @voto_total= 
        (select count(*) from VOTAEM where ZUF=old.ZUF and CanRG=old.CanRG and ZCodigo=old.ZCodigo);
        update HISTORICOVOTOS set votos=@voto_total
            where ano=old.ano and turno=old.turno and CanRG= old.CanRG and ZUF=old.ZUF and ZCodigo=old.ZCodigo;
    end if;
end $

#RESTRIÇÃO SEMANTICA: Um candidato nao pode votar em dois candidatos que concorrem para a mesma vaga
create trigger checa_voto_consistencia
before insert on VOTAEM
for each row
begin
    if exists(select * from VOTAEM where new.ERG=ERG and CanRG!=new.CanRG) then
        set @Candidato1 = (select Titulo from CANDIDATO where CanRG=new.CanRG);
        if exists( select CanRG from VOTAEM where new.ERG=ERG and (exists( select * from CANDIDATO where VOTAEM.CanRG=CANDIDATO.CanRG and @Candidato1 = CANDIDATO.Titulo )) ) then
            signal sqlstate '32662' set message_text='O eleitor ja votou para este cargo eletivo';
        end if;
    end if;    
end $

#Restrição Semântica: Após uma remoção de VOTAEM, é necessário esperar a remoção de todas suas tuplas antes de inserir novamente
create trigger trava_inserir_voto
before insert on VOTAEM
for each row
begin
	if (select Controle from variavelcontrole)=1 then
		signal sqlstate '32663' set message_text='Ainda há informações da ultima eleição na tabela';    
	end if;
end$

create trigger testa_fim_eleicao
after delete on VOTAEM
for each row
begin
	#Se existe tupla em VOTAEM, a eleicao ainda nao acabou, logo controle=1
	if exists(select* from VOTAEM) then
		update variavelcontrole
		set Controle=1;
	end if;
	if  not exists(select* from VOTAEM) then
		update variavelcontrole
		set Controle=0, controleano=0;
	end if;
end$

#Trigger para o calculo do historico final: quem venceu para cada cargo
create trigger preenche_Historico
after update on variavelcontrole
for each row
begin
	#Quando controleano=0, significa que a eleicao acabou e podemos calcular os vencedores
    if (select Controle from variavelcontrole)=0 and new.controleano<>1 then
    
    #acha o candidato a presidente com mais votos
    insert into historico(ano,canrg,titulo,votos,uf,zona,pcodigo)
    select Ano,Can,titulo,MAX(tvotos),(select UF from candidato where can=CanRG) as UF,Zcodigo,Pcodigo
    from(	select CanRG as can,Ano,SUM(votos)as tvotos,titulo,Zcodigo,Pcodigo
            from HISTORICOVOTOS
            group by CanRG,Ano
            having ano=(select ano from variavelcontrole)
            order by tvotos desc
            )as re where re.titulo='PE';
    #--------------
    #Acha os candidatos a governador que tem mais votos em cada estado
    insert into historico(ano,canrg,titulo,votos,uf,zona,pcodigo)
    select Ano,Can,titulo,Max(tvotos),UF,ZCodigo,Pcodigo
    from(select Ano,Can,titulo,tvotos,(select UF from candidato where can=CanRG) as UF,ZCodigo,Pcodigo
         from(  select CanRG as can,Ano,SUM(votos)as tvotos,titulo,ZCodigo,Pcodigo
                from HISTORICOVOTOS
                group by CanRG,Ano
                having ano=(select ano from variavelcontrole)
                order by tvotos desc) as Re
         where re.titulo in (select codigo from codcargo where codigo='GO' or codigo='SE' or codigo='DE' or codigo='DF') )as ree,coduf
    group by UF;
    #-------------------------------------------------------------------
	#Acha os candidatos a vereador e prefeito que possuem mais votos em sua zona eleitoral
    insert into historico 
    select Ano,Can,titulo,Max(tvotos), UF,ZCodigo,Pcodigo
	from(	select Ano,Can,titulo,tvotos,(select UF from candidato where can=CanRG) as UF, (select ZCodigo from eleitor where can=RG) as ZCodigo,Pcodigo
			from(	select CanRG as can,Ano,SUM(votos)as tvotos,titulo,Pcodigo
					from HISTORICOVOTOS
					group by CanRG,Ano
                    having ano=(select ano from variavelcontrole)
					order by tvotos desc) as Re
			where re.titulo in (select codigo from codcargo where codigo='VE' or codigo='PO') )as ree,coduf
	group by UF,Zcodigo;

    end if;
end$

#Atualizacao do ano para controlar a eleicao atual
create trigger att_ano
before insert on VOTAEM
for each row
begin
    if(select controleano from variavelcontrole)=0 then
		update variavelcontrole
			set ano=new.Ano, controleano=1;
    end if;
end$

#RESTRICAO SEMANTICA: Um eleitor nao pode se cadastrar em uma secao que nao faça parte de sua zona
create trigger zona_secao_consistencia
before insert on eleitor
for each row
begin
	if( not exists (select * from CONTEM where new.ZUF=ZUF and new.ZCodigo=ZCodigo and new.SCodigo=SCodigo)) then
		signal sqlstate '32664' set message_text='Esta secao nao esta cadastrada';    
    end if;
end$
create trigger zona_secao_consistencia_UPDATE
before update on eleitor
for each row
begin
	if( not exists (select * from CONTEM where new.ZUF=ZUF and new.ZCodigo=ZCodigo and new.SCodigo=SCodigo)) then
		signal sqlstate '32664' set message_text='Esta secao nao esta cadastrada';    
    end if;
end$

#RESTRIÇÃO SEMANTICA: Um mesario nao pode trabalhar em uma secao onde ele nao vota
create trigger  checa_mesario_consistencia
before insert on MESARIO
for each row
begin
	if new.ZUF<>(select ZUF from ELEITOR where new.RG=ELEITOR.RG) 
    or new.ZCodigo<>(select ZCodigo from ELEITOR where new.ZCodigo=ELEITOR.ZCodigo and new.RG=ELEITOR.RG )
    or new.SCodigo<>(select SCodigo from ELEITOR where new.SCodigo=ELEITOR.SCodigo and new.RG=ELEITOR.RG ) then
		signal sqlstate '32661' set message_text='O MESARIO está trabalhando fora de sua UF,zona ou seção';
    end if;
end $

DELIMITER ;